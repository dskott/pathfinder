#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>
#include <stdexcept>

using namespace std;

class route_planner {
	struct point {
		int x;
		int y;
		int value;
	};

	point origin, destination;
	vector< vector<int> > map;
	vector< vector<int> > allowed_junctions;
	// vector<vector<int>> neighbours = {{-1,-1},{-1,0},{-1,1},{0,-1},{0,1},{1,-1},{1,0},{1,1}}; //used for finding adjacent_points
	vector<vector<int>> neighbours = {{-1,0},{0,-1},{0,1},{1,0}};
	
	point set_point(int xcoord, int ycoord, int c) {
		//sets the elements of a struct point
		point this_point;
		this_point.x = xcoord;
		this_point.y = ycoord;
		this_point.value = c;
		return this_point;
	}

	//map functions
	bool is_valid(string input){
	//checks if the given line string has the correct matrix format
	  bool next_is_char = true;
	  for (int i = 0; i < input.length(); i++){
	    if (next_is_char){
	      switch (input[i]){
	        case '0':
	        case '1':
	          next_is_char = false;
	          break;
	        default:
	          return false;
	      }
	    } else {
	      switch (input[i]){
	        case ',':
	        case '\n':
	          next_is_char = true;
	          break;
	        default:
	          return false;
	      }
	    }
	  }
	  return true;
	}

	vector < vector<int>> get_map(string file_name){
		//reads the map saved in a txt file
		string line,number;
	  vector<vector<int>> matrix;
	  int row_length;

	  ifstream myfile (file_name);
	  if (myfile.is_open())
	  {
	  	//read each line from the txt file into a string
	    while ( getline (myfile,line) ) {
	    	if (!is_valid(line)) throw runtime_error("invalid input matrix: contains invalid characters");
	     	istringstream ss(line);
	     	vector<int> row;
	     	row.push_back(0); //add margin
	     	//break down each line into the individual numbers
	      while (getline(ss,number, ',')) {
	      	row.push_back(stoi(number));
	      }
	      row.push_back(0); //add margin
	      matrix.push_back(row);

	      //check input matrix is rectangular i.e. all rows have same length
	      if (matrix.size()==1) row_length = row.size();
	      else {
	      	if (row.size() != row_length) throw runtime_error("invalid input matrix: is not rectangular");
	      }
	    }
	    myfile.close();
	  }

	  else throw runtime_error("Unable to open file"); 

	  //add top and bottom margin
	  vector<int> margin(matrix[0].size(),0);
	  matrix.insert(matrix.begin(),margin);
	  matrix.insert(matrix.end(),margin);
	  return matrix;
	}
	void initialise_end_points(int originx, int originy, int destinationx, int destinationy){
		//checks that the given points lie within the map and are accessible (have value 1 in the map)
		//set origin and destination
		origin = set_point(originx, originy, 0);
		destination = set_point(destinationx, destinationy, 0);

		//size of map
		int height = allowed_junctions.size();
		int width = allowed_junctions[0].size();

		if ((originx < 0) || (originx > (height-1))) throw runtime_error("invalid origin and/or destination points");
		if ((originy < 0) || (originy > (width-1))) throw runtime_error("invalid origin and/or destination points");
		if (allowed_junctions[originx][originy] != 1) throw runtime_error("invalid origin and/or destination points");

		if ((destinationx < 0) || (destinationx > (height-1))) throw runtime_error("invalid origin and/or destination points");
		if ((destinationy < 0) || (destinationy > (width-1))) throw runtime_error("invalid origin and/or destination points");
		if (allowed_junctions[destinationx][destinationy] != 1) throw runtime_error("invalid origin and/or destination points");
	}


	//pathfinding functions
	bool is_same_point(point point_A, point point_B) {
		//returs true if pointA and pointB have the same x and y coordinates
		if ((point_A.x == point_B.x) && (point_A.y == point_B.y)) return true;
		else return false;
	}
	bool is_destination(point this_point, point destination) {
		//returns true if the point passed as an argument is the destination
		return is_same_point(this_point, destination);
	}
	vector<point> update_check_list(vector<point> possible_path, int counter) {
		//updates the check_list with all the points from the main_list that have point.value == counter
		//check_list.clear(); //clear the list first
		vector<point> check_list;
		//Search the possible_path vector and find all points with point.value == counter
		for (int i = possible_path.size() - 1; i >= 0; i--) {
			if (possible_path[i].value == counter) {
				check_list.push_back(possible_path[i]);
			}
		}
		return check_list;
	}
	
	vector<point> get_adjacent_points(point this_point, int counter, vector<vector<int>> map) {
		//gets the adjacent points around a point with coordinates (this_point.x,this_point.y), that have a value of 1 in the map
		int x;
		int y;
		vector<point> adjacent_points;

		//iterate over the neighbours vector so as to check the 8 neighbouring points
		for (int i = 0; i < neighbours.size(); i++){
			x = this_point.x + neighbours[i][0];
			y = this_point.y + neighbours[i][1];
			if (map[x][y] == 1) adjacent_points.push_back(set_point(x, y, counter));
		}

		return adjacent_points;
	}
	vector<point> add_adjacent_points_to_possible_path(vector<point> possible_path, vector<point> adjacent_points) {
		//adds the points from the adjacent_points vector to the possible_path vector, and removes duplicate points (with same x and y coordinate)

		//mark duplicate points 
		for (int i = possible_path.size() - 1; i >= 0; i--) {
			for (int j = adjacent_points.size() - 1; j >= 0; j--) {
				if (is_same_point(possible_path[i], adjacent_points[j])) adjacent_points[j].value = -1;
			}
		}

		//add non-duplicate points to possible_path 
		for (int j = adjacent_points.size() - 1; j >= 0; j--) {
			if (adjacent_points[j].value != -1) possible_path.push_back(adjacent_points[j]);
		}
		return possible_path;
	}
	vector<point> find_possible_path(point origin, point destination) {
		//main implementation of the pathfinding algorithm,
		//by moving iteratively from the origin and adding the adjacent cells to the possible_path vector, we reach the destination
		vector<point> check_list, possible_path;
		vector<point> adjacent_points; //adjacent points contains points in order: top, left, right, bottom
		point current_point;

		int counter = 0;
		current_point = set_point(origin.x, origin.y, counter);//get_current_point(origin[0], origin[1], counter);
		possible_path.push_back(current_point);
		check_list.push_back(current_point);
		counter++;

		while (!is_destination(current_point, destination)) {
			adjacent_points = get_adjacent_points(current_point, counter, allowed_junctions);
			possible_path = add_adjacent_points_to_possible_path(possible_path, adjacent_points);
			check_list.pop_back();
			if (check_list.empty()) {
				check_list = update_check_list(possible_path, counter);

				//check if we can proceed
				if (check_list.size() == 0) throw runtime_error("there is no possible path from origin to destination");

				counter++;
			}
			current_point = check_list.back();
		}
		return possible_path;
	}
	
	bool are_adjacent(point pointA, point pointB) {
		//returns true if the two points are next to each other
		if ((abs(pointA.x - pointB.x) == 1) && (abs(pointA.y - pointB.y) == 0)) return true;
		else if ((abs(pointA.x - pointB.x) == 0) && (abs(pointA.y - pointB.y) == 1)) return true;
		else return false;
	}
	// bool are_adjacent(point pointA, point pointB) {
	// 	//returns true if the two points are next to each other
	// 	//modification of the above function to allow for diagonal points
	// 	if ((abs(pointA.x - pointB.x) <= 1) && (abs(pointA.y - pointB.y) <= 1)) return true;
	// 	else return false;
	// }
	vector<point> extract_path(point origin, point destination) {
		//extracts only the relevant points from the possible_path vector
		
		//first get a possible_path vector
		vector<point> possible_path = find_possible_path(origin, destination);
		//now extract the relevant points, by starting from the destination point, and adding to the route vector, the points that are adjacent to the previous ones
		vector<point> route;
		route.push_back(destination);
		point previous_visited_point = destination;

		int counter; 
		//find the value of the counter of the destination point
		for (int i = possible_path.size() - 1; i >= 0; i--){
			if (is_destination(possible_path[i],destination)) counter = possible_path[i].value;
		}
		route[0].value = counter;

		for (int i = possible_path.size() - 1; i >= 0; i--) {
			if (possible_path[i].value >= counter) continue; //skip points visited after reaching the destination
			if (are_adjacent(possible_path[i], previous_visited_point)) {
				route.push_back(possible_path[i]);
				previous_visited_point = possible_path[i];
				counter = previous_visited_point.value;
			}
		}
		
		// //replace the point.value of each point with the values of the vector<point> map
		// for (int i = route.size() - 1; i >= 0; i--) {
		// 	route[i].value = map[route[i].x][route[i].y];
		// }
		return route;
	}


public:
	vector<point> route_found;

	//functions for  visualizing
	template <class type>
	void print_1dvector(vector<type> vec) {
		//prints a 1d vector
		for (typename vector<type>::const_iterator i = vec.begin(); i != vec.end(); ++i)
			cout << *i << ' ';
		cout << endl;
	}

	template <class type>
	void print_2dvector(vector< vector<type> > large_vec) {
		//prints a 2d vector
		for (typename vector< vector<type> >::const_iterator i = large_vec.begin();
			i != large_vec.end(); ++i) {
			print_1dvector<type>(*i);
		}
		cout << endl;
	}

	void print_vector_with_points(vector<point> vec) {
		//prints a vector that contains structs of points
		point this_point;
		for (vector<point>::const_iterator i = vec.begin(); i != vec.end(); ++i) {
			this_point = *i;
			cout << "(" << this_point.x << "," << this_point.y << "--> " << this_point.value << ")" << " ";
		}
		cout << endl;
	}

	vector<vector<string>> convert_to_string(vector<vector<int>> matrix){
	  //converts a matrix of ints to a matrix of strings

	  vector<vector<string>> string_matrix;

	  for (vector< vector<int> >::const_iterator i = matrix.begin();
	    i != matrix.end(); ++i) {
	    vector<int> row = *i;
	    vector<string> output_row;
	    for (vector<int>::const_iterator j = row.begin(); j != row.end(); ++j) {
	      int element = *j;

	      //modify the cases if the matrix contains values other than 0 and 1
	      switch (element){
	        case 1: 
	          output_row.push_back("-");
	          break;
	        default:
	          output_row.push_back("X");
	      }

	    }
	    string_matrix.push_back(output_row);
	  }
	  return string_matrix;
	}

	void visualize_path(vector<point> path) {
		//marks the points in the path vector on a copy of the allowed_junctions map
		vector<vector<string>> visualized_path =  convert_to_string(allowed_junctions);
		vector<vector<string>> visualized_map = visualized_path;

		//mark path
		int x, y, value;
		for (int i = path.size() - 1; i >= 0; i--) {
			x = path[i].x;
			y = path[i].y;
			visualized_path[x][y] = "o";
		}
		//mark origin and destination
		visualized_path[origin.x][origin.y] = "S";
		visualized_path[destination.x][destination.y] = "E";
		visualized_map[origin.x][origin.y] = "S";
		visualized_map[destination.x][destination.y] = "E";
		
		print_2dvector<string>(visualized_map);
		cout << endl;
		print_2dvector<string>(visualized_path);
		cout << "Size of map: " << visualized_map.size() << "x" << visualized_map[0].size() << endl;
		cout << endl;
	}

	//other functions
	void get_new_path(int originx, int originy, int destinationx, int destinationy) {
		//redefines origin and destination and allows the same object to calculate a different path
		initialise_end_points(originx, originy, destinationx, destinationy);

		//find route
		route_found = extract_path(origin, destination);
	}

	//constructor
	route_planner(int originx, int originy, int destinationx, int destinationy) {
		
		try {
			//intialise built-in map arrays in vector form
	  	allowed_junctions = get_map("map.txt");		
	  	map = allowed_junctions;
			
			initialise_end_points(originx, originy, destinationx, destinationy);
			//find route
			route_found = extract_path(origin, destination);

			visualize_path(route_found);
			print_vector_with_points(route_found);
	  } catch(const std::exception& e) {
			std::cout << "Caught exception: " << e.what() << '\n';
		}
		
	}
};


int main() {
	route_planner route ( 4,4,45,66); //change the argument of the constructor with other origin and destination points

	/*
	//Use this part of the code to find another path on the map with the same route_planner object, by redifining origin and destination point

	route.get_new_path(4, 1, 6, 5);
	route.print_vector_with_points(route.route_found);
	cout << endl;
	*/
}
