// reading a text file
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
using namespace std;


template <class type>
void print_1dvector(vector<type> vec) {
	//prints a 1d vector
	for (typename vector<type>::const_iterator i = vec.begin(); i != vec.end(); ++i)
		cout << *i << ' ';
	cout << endl;
}

template <class type>
void print_2dvector(vector< vector<type> > large_vec) {
	//prints a 2d vector
	for (typename vector< vector<type> >::const_iterator i = large_vec.begin();
		i != large_vec.end(); ++i) {
		print_1dvector<type>(*i);
	}
}

vector < vector<int>> get_map(string file_name){
	string line,number;
  vector<vector<int>> matrix;

  ifstream myfile (file_name);
  if (myfile.is_open())
  {
    while ( getline (myfile,line) ) {
     	istringstream ss(line);
     	vector<int> row;
      while (getline(ss,number, ',')) {
      	row.push_back(stoi(number));
      }
      matrix.push_back(row);
    }
    myfile.close();
  }

  else cout << "Unable to open file"; 

  return matrix;
}

vector<vector<string>> convert_to_string(vector<vector<int>> matrix){
  //converts a matrix of ints to a matrix of strings
  vector<vector<string>> string_matrix;
  for (vector< vector<int> >::const_iterator i = matrix.begin();
    i != matrix.end(); ++i) {
    vector<int> row = *i;
    vector<string> output_row;
    for (vector<int>::const_iterator j = row.begin(); j != row.end(); ++j) {
      int element = *j;
      switch (element){
        case 1: 
          output_row.push_back("_");
          break;
        default:
          output_row.push_back("X");
      }
    }
    string_matrix.push_back(output_row);
  }
  return string_matrix;
}

double division(int a, int b) {
  if( b == 0 ) {
    throw "Division by zero condition!";
  }
  return (a/b);
}

bool is_valid(string input){
  //checks if the given line string has the correct matrix format
  bool next_is_char = true;
  for (int i = 0; i < input.length(); i++){
    if (next_is_char){
      switch (input[i]){
        case '0':
        case '1':
          next_is_char = false;
          break;
        default:
          return false;
      }
    } else {
      switch (input[i]){
        case ',':
        case '\n':
          next_is_char = true;
          break;
        default:
          return false;
      }
    }
  }
  return true;
}

int main () {
  try {
    string mine = "1,0,1,1,1,1,1,2";
    if (!is_valid(mine)) throw runtime_error("invalid input matrix: contains invalid characters");
  } catch(const std::exception& e){
    cout << e.what() << endl;
  }
  
}